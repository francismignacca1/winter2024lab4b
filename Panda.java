public class Panda{
	
	private String name;
	private String species;
	private int averageLifeAge;
	
	public Panda(String name, String species, int averageLifeAge){
		this.name = name;
		this.species = species;
		this.averageLifeAge = averageLifeAge;
	}

	public void climb(){
		if(this.name.equals("The Giant Panda")){
			System.out.println("This type of panda only climb tree when they want to escape, are in danger, or want to rest");
		}
		else{
			System.out.println("They spend most of their time on trees, that's why they are excellent climbers");
		}
	}
	
	public void eat(){
		if(this.species.equals("Ailuropoda melanoleuca")){
			System.out.println("A giant panda's daily diet consists almost entirely of the leaves, stems and shoots of various bamboo species");
		}
		else{
			System.out.println("Red panda are obligate bamboo eaters.");
		}
	}
	
	
	public void setAverageLifeAge(int averageLifeAge){
		this.averageLifeAge = averageLifeAge;
	}
	public String getName(){
		return this.name;
	}
	public String getSpecies(){
		return this.species;
	}
	public int getAverageLifeAge(){
		return this.averageLifeAge;
	}

}
