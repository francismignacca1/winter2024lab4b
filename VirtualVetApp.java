import java.util.Scanner;
public class VirtualVetApp{
	public static void main (String[] args){
		Scanner reader = new Scanner(System.in);
		Panda[] pandas	= new Panda [4];
		
		
		
		for(int i=0; i<pandas.length; i++){
			System.out.println("Enter a name of the panda");
			String inputName = reader.nextLine();
			System.out.println("Enter one of the two species of the panda");
			String inputSpecies = reader.nextLine();
			System.out.println("Enter the average life age of the panda in numbers");
			int inputAge = Integer.parseInt(reader.nextLine());
			pandas[i] = new Panda(inputName, inputSpecies, inputAge);
		}
		System.out.println(pandas[pandas.length-1].getName());
		System.out.println(pandas[pandas.length-1].getSpecies());
		System.out.println(pandas[pandas.length-1].getAverageLifeAge());
		
		System.out.println("Enter the average life age of the panda in numbers");
		int inputAge2 = Integer.parseInt(reader.nextLine());
		pandas[pandas.length-1].setAverageLifeAge(inputAge2);
		System.out.println(pandas[pandas.length-1].getName());
		System.out.println(pandas[pandas.length-1].getSpecies());
		System.out.println(pandas[pandas.length-1].getAverageLifeAge());
		
		
	}
}
